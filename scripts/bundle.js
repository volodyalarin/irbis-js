/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/components/App.tsx":
/*!********************************!*\
  !*** ./src/components/App.tsx ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ App)\n/* harmony export */ });\n/* harmony import */ var irbis__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! irbis */ \"./src/irbis/index.ts\");\n/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header */ \"./src/components/Header.tsx\");\n/* harmony import */ var irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! irbis/jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\n\n\n\n\nclass App extends irbis__WEBPACK_IMPORTED_MODULE_0__.IrbisComponent {\n  render() {\n    return (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(\"div\", {\n      children: [(0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_Header__WEBPACK_IMPORTED_MODULE_1__.default, {\n        name: \"Irbis\",\n        links: [\"This\", \"is\", \"for\", \"loop\"],\n        children: \"This is header\"\n      }), this.renderSlot()]\n    });\n  }\n\n}\n\n//# sourceURL=webpack:///./src/components/App.tsx?");

/***/ }),

/***/ "./src/components/Counter.tsx":
/*!************************************!*\
  !*** ./src/components/Counter.tsx ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Counter)\n/* harmony export */ });\n/* harmony import */ var irbis__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! irbis */ \"./src/irbis/index.ts\");\n/* harmony import */ var irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! irbis/jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\n\n\n\nclass Counter extends irbis__WEBPACK_IMPORTED_MODULE_0__.IrbisComponent {\n  inc() {\n    this.props.counter++;\n  }\n\n  dec() {\n    this.props.counter--;\n  }\n\n  render() {\n    return (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(\"div\", {\n      children: [(0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"button\", {\n        onclick: () => {\n          this.inc();\n        },\n        children: \"+\"\n      }), (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"button\", {\n        onclick: () => {\n          this.dec();\n        },\n        children: \"-\"\n      }), this.props.counter]\n    });\n  }\n\n}\n\n//# sourceURL=webpack:///./src/components/Counter.tsx?");

/***/ }),

/***/ "./src/components/Header.tsx":
/*!***********************************!*\
  !*** ./src/components/Header.tsx ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Header)\n/* harmony export */ });\n/* harmony import */ var irbis__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! irbis */ \"./src/irbis/index.ts\");\n/* harmony import */ var irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! irbis/jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\n\n\n\nclass Header extends irbis__WEBPACK_IMPORTED_MODULE_0__.IrbisComponent {\n  render() {\n    return (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(\"header\", {\n      class: \"d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom\",\n      children: [(0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"a\", {\n        href: \"/\",\n        class: \"d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none\",\n        children: (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"span\", {\n          class: \"fs-4\",\n          children: this.props.name\n        })\n      }), (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"ul\", {\n        class: \"nav nav-pills\",\n        children: this.props.links.map(link => {\n          return (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"li\", {\n            class: \"nav-item\",\n            children: (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"a\", {\n              href: \"#\",\n              class: \"nav-link active\",\n              onclick: () => {\n                console.log(\"Hello from link\" + link);\n              },\n              children: link\n            })\n          });\n        })\n      })]\n    });\n  }\n\n}\n\n//# sourceURL=webpack:///./src/components/Header.tsx?");

/***/ }),

/***/ "./src/components/Post.tsx":
/*!*********************************!*\
  !*** ./src/components/Post.tsx ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Post)\n/* harmony export */ });\n/* harmony import */ var irbis__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! irbis */ \"./src/irbis/index.ts\");\n/* harmony import */ var irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! irbis/jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }\n\nfunction _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err); } _next(undefined); }); }; }\n\n\n\n\nclass Post extends irbis__WEBPACK_IMPORTED_MODULE_0__.IrbisComponent {\n  created() {\n    var _this = this;\n\n    return _asyncToGenerator(function* () {\n      var data = yield fetch('https://jsonplaceholder.typicode.com/posts/1');\n      var json = yield data.json();\n      _this.props.title = json.title;\n      _this.props.body = json.body;\n      _this.props.loaded = true;\n    })();\n  }\n\n  render() {\n    return (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(\"div\", {\n      class: \"card\",\n      children: [!this.props.loaded ? (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(\"div\", {\n        class: \"spinner-border\",\n        role: \"status\"\n      }) : null, (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(\"h1\", {\n        children: [\" \", this.props.title]\n      }), this.props.body]\n    });\n  }\n\n}\n\n//# sourceURL=webpack:///./src/components/Post.tsx?");

/***/ }),

/***/ "./src/index.tsx":
/*!***********************!*\
  !*** ./src/index.tsx ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var irbis__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! irbis */ \"./src/irbis/index.ts\");\n/* harmony import */ var _components_App_tsx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/App.tsx */ \"./src/components/App.tsx\");\n/* harmony import */ var _components_Counter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Counter */ \"./src/components/Counter.tsx\");\n/* harmony import */ var _components_Post__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/Post */ \"./src/components/Post.tsx\");\n/* harmony import */ var irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! irbis/jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\n\n\n\n\n\n\nvar app = new irbis__WEBPACK_IMPORTED_MODULE_0__.default((0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(\"div\", {\n  children: (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_components_App_tsx__WEBPACK_IMPORTED_MODULE_1__.default, {\n    children: [(0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(\"div\", {\n      children: (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(\"h1\", {\n        children: \"This is slot of app\"\n      })\n    }), (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_components_Counter__WEBPACK_IMPORTED_MODULE_2__.default, {\n      counter: 5\n    }), (0,irbis_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_components_Post__WEBPACK_IMPORTED_MODULE_3__.default, {})]\n  })\n}));\napp.mount(\"body\");\n\n//# sourceURL=webpack:///./src/index.tsx?");

/***/ }),

/***/ "./src/irbis/core/index.ts":
/*!*********************************!*\
  !*** ./src/irbis/core/index.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Irbis\": () => (/* binding */ Irbis)\n/* harmony export */ });\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nclass Irbis {\n  constructor(root) {\n    _defineProperty(this, \"dom_element\", void 0);\n\n    _defineProperty(this, \"root\", void 0);\n\n    _defineProperty(this, \"rendered_root\", null);\n\n    this.root = root;\n  }\n\n  forceUpdate() {\n    this.dom_element.innerHTML = '';\n    this.dom_element.appendChild(this.root.createElement());\n  }\n\n  mount(selector) {\n    this.dom_element = document.querySelector(selector);\n    this.forceUpdate();\n  }\n\n}\n\n//# sourceURL=webpack:///./src/irbis/core/index.ts?");

/***/ }),

/***/ "./src/irbis/differ.ts":
/*!*****************************!*\
  !*** ./src/irbis/differ.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"DiffEnum\": () => (/* binding */ DiffEnum),\n/* harmony export */   \"differ\": () => (/* binding */ deepDiffMapper)\n/* harmony export */ });\nvar DiffEnum;\n\n(function (DiffEnum) {\n  DiffEnum[\"VALUE_CREATED\"] = \"created\";\n  DiffEnum[\"VALUE_UPDATED\"] = \"updated\";\n  DiffEnum[\"VALUE_DELETED\"] = \"deleted\";\n  DiffEnum[\"VALUE_UNCHANGED\"] = \"unchanged\";\n})(DiffEnum || (DiffEnum = {}));\n\nclass deepDiffMapper {\n  map(obj1, obj2) {\n    if (this.isFunction(obj1) || this.isFunction(obj2)) {\n      throw 'Invalid argument. Function given, object expected.';\n    }\n\n    if (this.isValue(obj1) || this.isValue(obj2)) {\n      return {\n        type: this.compareValues(obj1, obj2),\n        data: obj1 === undefined ? obj2 : obj1\n      };\n    }\n\n    var diff = {};\n\n    for (var key in obj1) {\n      if (this.isFunction(obj1[key])) {\n        continue;\n      }\n\n      var value2 = undefined;\n\n      if (obj2[key] !== undefined) {\n        value2 = obj2[key];\n      }\n\n      diff[key] = this.map(obj1[key], value2);\n    }\n\n    for (var key in obj2) {\n      if (this.isFunction(obj2[key]) || diff[key] !== undefined) {\n        continue;\n      }\n\n      diff[key] = this.map(undefined, obj2[key]);\n    }\n\n    return diff;\n  }\n\n  compareValues(value1, value2) {\n    if (value1 === value2) {\n      return DiffEnum.VALUE_UNCHANGED;\n    }\n\n    if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {\n      return DiffEnum.VALUE_UNCHANGED;\n    }\n\n    if (value1 === undefined) {\n      return DiffEnum.VALUE_CREATED;\n    }\n\n    if (value2 === undefined) {\n      return DiffEnum.VALUE_DELETED;\n    }\n\n    return DiffEnum.VALUE_UPDATED;\n  }\n\n  isFunction(x) {\n    return Object.prototype.toString.call(x) === '[object Function]';\n  }\n\n  isArray(x) {\n    return Object.prototype.toString.call(x) === '[object Array]';\n  }\n\n  isDate(x) {\n    return Object.prototype.toString.call(x) === '[object Date]';\n  }\n\n  isObject(x) {\n    return Object.prototype.toString.call(x) === '[object Object]';\n  }\n\n  isValue(x) {\n    return !this.isObject(x) && !this.isArray(x);\n  }\n\n}\n\n\n\n//# sourceURL=webpack:///./src/irbis/differ.ts?");

/***/ }),

/***/ "./src/irbis/dom/component.ts":
/*!************************************!*\
  !*** ./src/irbis/dom/component.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"IrbisComponent\": () => (/* binding */ IrbisComponent)\n/* harmony export */ });\n/* harmony import */ var _node__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node */ \"./src/irbis/dom/node.ts\");\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\nclass IrbisComponent {\n  constructor() {\n    _defineProperty(this, \"slot\", null);\n\n    _defineProperty(this, \"props\", null);\n\n    _defineProperty(this, \"node\", null);\n\n    _defineProperty(this, \"tree\", null);\n\n    this.created();\n  }\n\n  setNode(node) {\n    this.node = node;\n  }\n\n  setSlot(slot) {\n    this.slot = slot;\n  }\n\n  created() {}\n\n  _createProps(data) {\n    return new Proxy(data, {\n      set: (target, prop, val) => {\n        target[prop] = val;\n        this.needUpdate();\n        return true;\n      },\n\n      get(target, prop) {\n        return target[prop];\n      }\n\n    });\n  }\n\n  setProps(props) {\n    this.props = this._createProps(props);\n  }\n\n  renderSlot() {\n    return this.slot;\n  }\n\n  needUpdate() {\n    console.log(\"need update\", this);\n    (0,_node__WEBPACK_IMPORTED_MODULE_0__.NodeDiffHandler)(this.tree, this.render());\n  }\n\n  render() {\n    return null;\n  }\n\n}\n\n//# sourceURL=webpack:///./src/irbis/dom/component.ts?");

/***/ }),

/***/ "./src/irbis/dom/node.ts":
/*!*******************************!*\
  !*** ./src/irbis/dom/node.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"IrbisVDOMNode\": () => (/* binding */ IrbisVDOMNode),\n/* harmony export */   \"NodeDiffHandler\": () => (/* binding */ NodeDiffHandler)\n/* harmony export */ });\n/* harmony import */ var _differ__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../differ */ \"./src/irbis/differ.ts\");\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\nvar IrbisBasicComponentEnum;\n\n(function (IrbisBasicComponentEnum) {\n  IrbisBasicComponentEnum[\"Text\"] = \"@text\";\n})(IrbisBasicComponentEnum || (IrbisBasicComponentEnum = {}));\n\nclass IrbisVDOMNode {\n  constructor(element, attributes, children) {\n    _defineProperty(this, \"component\", void 0);\n\n    _defineProperty(this, \"children\", void 0);\n\n    _defineProperty(this, \"attributes\", void 0);\n\n    _defineProperty(this, \"eventHandlers\", {});\n\n    _defineProperty(this, \"dom\", void 0);\n\n    _defineProperty(this, \"parent\", null);\n\n    if (typeof element == \"string\") {\n      this.component = element;\n    } else {\n      this.component = new element();\n    }\n\n    for (var _key in attributes) {\n      if (_key.startsWith(\"on\")) {\n        this.eventHandlers[_key.substr(2)] = attributes[_key];\n        delete attributes[_key];\n      }\n    }\n\n    this.attributes = attributes;\n    this.children = children.map(child => {\n      if (typeof child == \"string\" || typeof child == \"number\") return new IrbisVDOMNode(IrbisBasicComponentEnum.Text, {\n        text: child\n      }, []);\n      if (typeof child == \"undefined\" || typeof child == \"null\") return new IrbisVDOMNode(IrbisBasicComponentEnum.Text, {\n        text: \"\"\n      }, []);\n      return child;\n    });\n    this.children.forEach(child => {\n      if (child) child.parent = this;\n    });\n\n    if (typeof this.component == \"object\") {\n      this.component.setSlot(this.children);\n      this.component.setProps(this.attributes);\n      this.component.setNode(this);\n    }\n  }\n\n  _createBasicElement(node) {\n    if (typeof node.component != \"string\") return;\n\n    if (node.component == IrbisBasicComponentEnum.Text) {\n      return document.createTextNode(node.attributes.text);\n    }\n\n    var element = document.createElement(node.component);\n\n    for (var atr in this.attributes) {\n      element.setAttribute(atr, this.attributes[atr]);\n    }\n\n    console.log(node, node.children);\n\n    this._processChildren(node.children, element);\n\n    for (var event in this.eventHandlers) {\n      element.addEventListener(event, this.eventHandlers[event]);\n    }\n\n    return element;\n  }\n\n  _processChildren(children, element) {\n    children.forEach(child => {\n      if (!child) return;\n\n      if (Array.isArray(child)) {\n        return this._processChildren(child, element);\n      }\n\n      var childElement = child.createElement();\n      element.appendChild(childElement);\n    });\n  }\n\n  _createElement(node) {\n    if (typeof node.component == \"string\") {\n      return node._createBasicElement(node);\n    }\n\n    node.component.tree = node.component.render();\n    console.log(node);\n    var x = node.component.tree.createElement();\n    return x;\n  }\n\n  createElement() {\n    this.dom = this._createElement(this);\n    return this.dom;\n  }\n\n}\nfunction NodeDiffHandler(old, n) {\n  if (!n && !old) return;\n\n  if (!old && n) {\n    console.log('I need help', old, n);\n    return;\n  }\n\n  if (old && !n) {\n    old.dom.remove();\n    return;\n  }\n\n  n.dom = old.dom;\n\n  if (typeof old.component != typeof n.component || typeof old.component == \"string\" && old.component.constructor.name != n.component.constructor.name) {\n    var element = n.createElement();\n    old.dom.replaceWith(element);\n    return n;\n  }\n\n  if (typeof old.component == \"string\") {\n    if (old.component == IrbisBasicComponentEnum.Text) {\n      if (old.attributes.text != n.attributes.text) {\n        old.dom.nodeValue = n.attributes.text;\n        old.attributes.text = n.attributes.text;\n      }\n\n      return old;\n    }\n\n    var diff = new _differ__WEBPACK_IMPORTED_MODULE_0__.differ().map(old.attributes, n.attributes);\n\n    for (var atr in diff) {\n      var _element = old.dom;\n\n      if (diff[atr].type == _differ__WEBPACK_IMPORTED_MODULE_0__.DiffEnum.VALUE_CREATED || diff[atr].type == _differ__WEBPACK_IMPORTED_MODULE_0__.DiffEnum.VALUE_UPDATED) {\n        _element.setAttribute(atr, diff[atr].data);\n      }\n\n      if (diff[atr].type == _differ__WEBPACK_IMPORTED_MODULE_0__.DiffEnum.VALUE_DELETED) {\n        _element.removeAttribute(atr);\n      }\n    }\n\n    var min = Math.min(old.children.length, n.children.length);\n    var max = Math.max(old.children.length, n.children.length);\n\n    for (var i = 0; i < max; ++i) {\n      if (i >= min) {\n        if (old.children[i]) {\n          old.children[i].dom.remove();\n        }\n\n        if (n.children[i]) {\n          n.dom.appendChild(n.children[i].createElement());\n        }\n\n        continue;\n      }\n\n      old.children[i] = NodeDiffHandler(old.children[i], n.children[i]);\n    }\n\n    return old;\n  }\n\n  if (typeof n.component == \"string\") return;\n  NodeDiffHandler(old.component.tree, n.component.render());\n  return n;\n}\n\n//# sourceURL=webpack:///./src/irbis/dom/node.ts?");

/***/ }),

/***/ "./src/irbis/index.ts":
/*!****************************!*\
  !*** ./src/irbis/index.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"IrbisComponent\": () => (/* reexport safe */ _dom_component__WEBPACK_IMPORTED_MODULE_1__.IrbisComponent),\n/* harmony export */   \"jsx\": () => (/* reexport safe */ _jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx),\n/* harmony export */   \"jsxs\": () => (/* reexport safe */ _jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ \"./src/irbis/core/index.ts\");\n/* harmony import */ var _dom_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dom/component */ \"./src/irbis/dom/component.ts\");\n/* harmony import */ var _jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./jsx-runtime */ \"./src/irbis/jsx-runtime.ts\");\n\n\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_core__WEBPACK_IMPORTED_MODULE_0__.Irbis);\n\n//# sourceURL=webpack:///./src/irbis/index.ts?");

/***/ }),

/***/ "./src/irbis/jsx-runtime.ts":
/*!**********************************!*\
  !*** ./src/irbis/jsx-runtime.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"jsxs\": () => (/* binding */ jsxs),\n/* harmony export */   \"jsx\": () => (/* binding */ jsx)\n/* harmony export */ });\n/* harmony import */ var _dom_node__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom/node */ \"./src/irbis/dom/node.ts\");\n\nfunction jsxs(element, attributes) {\n  var children = attributes.children || [];\n  delete attributes.children;\n  return new _dom_node__WEBPACK_IMPORTED_MODULE_0__.IrbisVDOMNode(element, attributes, children);\n}\nfunction jsx(element, attributes) {\n  var children = [attributes.children] || 0;\n  delete attributes.children;\n  return new _dom_node__WEBPACK_IMPORTED_MODULE_0__.IrbisVDOMNode(element, attributes, children);\n}\n\n//# sourceURL=webpack:///./src/irbis/jsx-runtime.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.tsx");
/******/ 	
/******/ })()
;