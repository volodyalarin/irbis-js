const path = require('path');

module.exports = {
    mode: 'development',
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, './scripts/'),
        filename: 'bundle.js',
    },

    resolve: {
        modules: [path.resolve(__dirname, 'src'), 'node_modules'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },

    module: {
        rules: [
            {
                test: /(.tsx)|(.ts)|(.js)|(.jsx)/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": [
                            "@babel/preset-typescript",
                            [
                                "@babel/preset-env",
                                {
                                    "modules": false,
                                    "targets": {
                                        "chrome": "58",
                                        "esmodules": true
                                    }
                                }
                            ]
                        ],
                        "plugins": [
                            "@babel/plugin-syntax-jsx",
                            [
                                "@babel/plugin-transform-react-jsx",
                                {
                                    "throwIfNamespace": false,
                                    "runtime": "automatic",
                                    "importSource": "irbis"
                                }
                            ],

                            "@babel/plugin-transform-react-display-name"
                        ]
                    }
                }
            }
        ]
    },

}