import { IrbisComponent } from "irbis";


export default class TreeComponentView extends IrbisComponent {
  // props: nodes
  render() {
    return (
      <ul class="list-group">
        hello
        {
          this.props.nodes.forEach(node => {
            return <li class="list-group-item">
              {node.name()}
              <TreeComponentView nodes={node.slot}/>
            </li>;
          })
        }
      </ul>
    );
  }
}
