import { IrbisComponent } from "irbis";

import Header from "./Header";

export default class Tabs extends IrbisComponent {
  data() {
    return {
      current: "a",
      tabsNames: ["a", "b", "c"],
    };
  }
  render() {
    return (
      <div>
        {this.props.tabsNames.map((name) => {
          return (
            <button
              class="btn btn-primary"
              onclick={() => {
                this.props.current = name;
              }}
            >
              {name}
            </button>
          );
        })}
        {(() => {
          if (this.props.current == "a") {
            return <div class="card">Tab a</div>;
          }
          if (this.props.current == "b") {
            return (
              <div class="card">
                Tab b{" "}
                <div class="alert alert-danger" role="alert">
                  A simple danger alert—check it out!
                </div>
              </div>
            );
          }
          if (this.props.current == "c") {
            return (
              <div class="card">
                Tab c
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                      <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                      <a href="#">Library</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                      Data
                    </li>
                  </ol>
                </nav>
              </div>
            );
          }
          return <p>No tab</p>;
        })()}
      </div>
    );
  }
}
