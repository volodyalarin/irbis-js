import { IrbisComponent } from "irbis";

import Header from "./Header";

export default class Post extends IrbisComponent {
  data() {
    return {
      posts: [],
    };
  }
  async created() {
    let data = await fetch("https://jsonplaceholder.typicode.com/posts");
    let json = await data.json();

    this.props.posts = json;
    this.props.loaded = true;
  }
  render() {
    return (
      <div>
        {!this.props.loaded ? (
          <div class="spinner-border" role="status"></div>
        ) : null}

        {this.props.posts.map((post) => {
          return (
            <div class="card p-2 mb-2" >
              <div class="display-6 mb-1"> {post.title}</div>
              {post.body}
            </div>
          );
        })}
      </div>
    );
  }
}
