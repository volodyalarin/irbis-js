import { IrbisComponent } from "irbis";

import Header from "./Header";

export default class App extends IrbisComponent {
  render() {
    return (
      <div>
        <Header name="Irbis" links={["This", "is", "for", "loop"]}></Header>
        <div class="container">
          <div class="container">{this.renderSlot()}</div>
        </div>
      </div>
    );
  }
}
