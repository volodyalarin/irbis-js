import { IrbisComponent } from "irbis";

export default class Counter extends IrbisComponent
{
    inc(){
        this.props.counter++;
    }

    dec(){
        this.props.counter--;
    }

    input(e) {
        this.props.counter = e.target.value
    }
    render() {
        return <div>
            <button onclick={()=>{this.inc()}}>+</button>
            <button onclick={()=>{this.dec()}}>-</button>

            <input type="text" value={this.props.counter} oninput={(e)=>{this.input(e)}} />
            <span style='margin-left:10px'>
            {this.props.counter}
                </span>
        </div>
        
    }
}