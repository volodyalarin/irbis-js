import { IrbisComponent } from "irbis";
import TreeComponentView from "./TreeComponentView";

export default class MainTree extends IrbisComponent {
  // props: app
  render() {
    return (
      <div>
        <TreeComponentView nodes={[this.props.app.root]} />
      </div>
    );
  }
}
