import { IrbisComponent } from "irbis";

export default class Header extends IrbisComponent {
  render() {
    return (
      <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
        <a
          href="/"
          class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none"
        >
          <span class="fs-4">{this.props.name}</span>
        </a>
        <ul class="nav nav-pills">
          {this.props.links.map((link) => {
            return (
              <li class="nav-item">
                <a
                  href="#"
                  class="nav-link active"
                  onclick={()=>{console.log("Hello from link" + link)}}
                >
                  {link}
                </a>
              </li>
            );
          })}
        </ul>
      </header>
    );
  }
}
