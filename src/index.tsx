import Irbis from "irbis";
import App from "./components/App.tsx";
import Counter from "./components/Counter";
import Post from "./components/Post";
import Tabs from "./components/Tabs";
import MainTree from "./components/MainTree";
import { IrbisBaseComponent } from "./irbis/dom/component";

let app = new Irbis(
  (
    <div>
      <App>
        <h1>Это слот App</h1>
        <p>Счетчик</p>
        <Counter counter={5} />
        <p></p>
        <p>Вкладки</p>
        <Tabs></Tabs>
        <p></p>
        <p>Подгруженные асинхронно записи</p>
        <Post></Post>
      </App>
    </div>
  )
);

app.mount("#app");

console.log(app);

// let treeView = new Irbis(<MainTree app={app} />);
// treeView.mount("#treeView");

document.querySelector("#treeView").innerHTML = `
<button id="treeViewUpdate"> Обновить дерево компонентов </button>
<button id="mainUpdateForce"> Обновить движок принудительно </button>
<button id="closeDevPanel"> Закрыть панель отладки </button>
<div id="treeViewUpdateBlock"></div>

`;
document.querySelector("#mainUpdateForce").addEventListener("click", () => {
  app.forceUpdate();
})
document.querySelector("#closeDevPanel").addEventListener("click", () => {
  document.querySelector("#treeView").remove();
})

document.querySelector("#treeViewUpdate").addEventListener("click", () => {
  let level = 0;
  let result = [];
  let divider = (level: number) => {
    let divider = "";
    for (let i = 0; i < level; ++i) {
      divider += "--";
    }
    return divider;
  };

  let walker = (node: IrbisBaseComponent) => {
    let args = [];
    for (let arg in node.props) {
      if (arg.startsWith("on")) {
        args.push(arg);
        continue;
      }
      args.push(arg + ":" + String(node.props[arg]));
    }
    let str = divider(level) + node.name() + " [" + args.join(";") + "]";

    result.push(str);

    level++;
    if (node.slot.length)
      result.push(divider(level - 1) + "> SLOT");
    node.slot.forEach(walker);

    if (node.rendered_root) {
      result.push(divider(level - 1) + "> Rendered");

      walker(node.rendered_root);
    }

    level--;
  };
  walker(app.root);
  document.querySelector("#treeViewUpdateBlock").innerHTML =
    "<pre>" + result.join("\n") + "</pre>";
});

var clickEvent = new MouseEvent("click", {
  "view": window,
  "bubbles": true,
  "cancelable": false
});

document.querySelector("#treeViewUpdate").dispatchEvent(clickEvent);