import { Irbis } from "./core";

import { IrbisComponent } from "./dom/component";

import {jsx, jsxs} from "./jsx-runtime"

export {IrbisComponent, jsx, jsxs};
export default Irbis;
