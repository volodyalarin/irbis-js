export enum DiffEnum {
    VALUE_CREATED = 'created',
    VALUE_UPDATED = 'updated',
    VALUE_DELETED = 'deleted',
    VALUE_UNCHANGED = 'unchanged'
}

export type DiffData = {
    type: DiffEnum;
    data: any
}

export type DiffRes = {
    [key: string]: DiffData
}

class deepDiffMapper {
    mapObject(obj1:object, obj2: object): DiffRes {
        return this.map(obj1, obj2)
    }
    map (obj1, obj2): DiffRes | DiffData {
        if (this.isFunction(obj1) || this.isFunction(obj2)) {
            throw 'Invalid argument. Function given, object expected.';
        }
        if (this.isValue(obj1) || this.isValue(obj2)) {
            return {
                type: this.compareValues(obj1, obj2),
                data: obj1 === undefined ? obj2 : obj1
            };
        }

        var diff = {};
        for (var key in obj1) {
            if (this.isFunction(obj1[key])) {
                continue;
            }

            var value2 = undefined;
            if (obj2[key] !== undefined) {
                value2 = obj2[key];
            }

            diff[key] = this.map(obj1[key], value2);
        }
        for (var key in obj2) {
            if (this.isFunction(obj2[key]) || diff[key] !== undefined) {
                continue;
            }

            diff[key] = this.map(undefined, obj2[key]);
        }

        return diff;

    }

    compareValues (value1, value2): DiffEnum {
        if (value1 === value2) {
            return DiffEnum.VALUE_UNCHANGED;
        }
        if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {
            return DiffEnum.VALUE_UNCHANGED;
        }
        if (value1 === undefined) {
            return DiffEnum.VALUE_CREATED;
        }
        if (value2 === undefined) {
            return DiffEnum.VALUE_DELETED;
        }
        return DiffEnum.VALUE_UPDATED;
    }
    
    isFunction (x) {
        return Object.prototype.toString.call(x) === '[object Function]';
    }
    isArray (x) {
        return Object.prototype.toString.call(x) === '[object Array]';
    }
    isDate (x) {
        return Object.prototype.toString.call(x) === '[object Date]';
    }
    isObject (x) {
        return Object.prototype.toString.call(x) === '[object Object]';
    }
    isValue (x) {
        return !this.isObject(x) && !this.isArray(x);
    }
}

export { deepDiffMapper as differ}