export enum objectDifferenceEnum {
    UPDATED = "updated",
    DELETED = "deleted",
    ADDED = "added",
}

type objectDifferenceEntry = {
    key: string;
    type: objectDifferenceEnum;
}

let utils = {
    objectDifference(left: object, right: object): objectDifferenceEntry[] {
        let updates: objectDifferenceEntry[] = [];

        for (let key in left) {
            if (right[key] && right[key] != left[key]) {
                updates.push({
                    key: key,
                    type: objectDifferenceEnum.UPDATED
                })
            } else if (!right[key]) {
                updates.push({
                    key: key,
                    type: objectDifferenceEnum.DELETED
                })
            }
        }
        for (let key in right) {
            if (!left[key]) {
                updates.push({
                    key: key,
                    type: objectDifferenceEnum.ADDED
                })
            }
        }
        return updates;
    }
}

export {utils}