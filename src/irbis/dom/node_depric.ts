import { IrbisComponent, BasicComponent } from "./component"
import { differ, DiffRes, DiffEnum } from "../differ";
import { Irbis } from "../core";

enum IrbisBasicComponentEnum {
    Text = "@text",
    Input = "input",
    Template = "templateIrbis"
}

type handlersType = {
    [key: string]: Function;
};

export class IrbisVDOMNode {
    component: IrbisComponent;
    dom: Element | Text
    parent: IrbisVDOMNode = null;
    irbis: Irbis = null

    setIrbis(irbis: Irbis) {
        this.irbis = irbis;

        this.children.forEach((child) => {
            if (child)
                child.setIrbis(irbis)
        })

        this.component.setIrbis(irbis);
    }


    constructor(element: any, attributes: any, children: (IrbisVDOMNode | string)[]) {
        if (typeof (element) == "string") {
            this.component = new BasicComponent(element);
        } else {
            this.component = new element;
        }

        for (let key in attributes) {
            if (key.startsWith("on")) {
                this.eventHandlers[key.substr(2)] = attributes[key];
                delete attributes[key]
            }
        }
        this.attributes = attributes;

        this.children = children.map((child) => {
            if (typeof (child) == "string" || typeof (child) == "number")
                return new IrbisVDOMNode(IrbisBasicComponentEnum.Text, {
                    text: child
                }, [])
            if (typeof (child) == "undefined" || typeof (child) == "null")
                return new IrbisVDOMNode(IrbisBasicComponentEnum.Text, {
                    text: ""
                }, [])
            if (Array.isArray(child))
                return new IrbisVDOMNode(IrbisBasicComponentEnum.Template, {
                    children: child
                }, [])
            return child
        });

        this.children.forEach((child) => {
            if (child) {
                child.parent = this
            }

        })


        this.component.setSlot(this.children);
        this.component.setProps(this.attributes);
        this.component.setNode(this);
    }

    _createBasicElement(): Element | Text {
        if (!this.component.isBasic()) return;

        if (this.component.name() == IrbisBasicComponentEnum.Text) {
            return document.createTextNode(this.attributes.text)
        }

        let element = document.createElement(this.component.name());

        for (let atr in this.attributes) {
            this._setDomAttribute(element, atr, this.attributes[atr])
        }

        console.log(this, this.children)
        this._processChildren(this.children, element);

        for (let event in this.eventHandlers) {
            element.addEventListener(event, this.eventHandlers[event]);
        }

        return element;
    }

    private _processChildren(children: IrbisVDOMNode[], element: HTMLElement) {
        children.forEach((child) => {
            if (!child)
                return;
            if (Array.isArray(child)) {
                console.warn("Depricated")
                return this._processChildren(child, element)
            }

            let childElement = child.createElement();
            element.appendChild(childElement);
        });
    }

    createElement(): Element | Text {
        this.component.tree = this.component.render();
        this.component.tree.setIrbis(this.irbis);

        this.dom = this.component.tree.createElement()

        return this.dom;
    }

    _setDomAttribute(element: Element, name: string, value: any) {
        if (element.tagName == 'INPUT' && name == 'value') {
            console.log('Im there', arguments);

            return element.value = value;

        }
        element.setAttribute(name, value);

    }
}
export function NodeArraysDiffs(old: IrbisVDOMNode[], n: IrbisVDOMNode[]) {
    let min = Math.min(old.length, n.length)
    let max = Math.max(old.length, n.length)
    for (let i = 0; i < max; ++i) {
        if (i >= min) {
            if (old[i]) {
                old[i].dom.remove();
            }

            if (n[i]) {
                n[i].parent.dom.appendChild(n[i].createElement());
                old[i] = n[i]
            }
            continue;
        }

        old[i] = NodeDiffHandler(old[i], n[i]);
    }
}

export function NodeDiffHandler(old: IrbisVDOMNode, n: IrbisVDOMNode): IrbisVDOMNode {
    if (!n && !old) return;
    if (!old && n) {
        return
    }
    if (old && !n) {
        old.dom.remove();
        return;
    }

    n.dom = old.dom;

    if (
        old.component.isBasic() && n.component.isBasic() &&
        old.component.name() != n.component.name()
    ) {
        let element = n.createElement();
        old.dom.replaceWith(element);
        return n;
    }

    if (old.component.isBasic()) {
        if (old.component.name() == IrbisBasicComponentEnum.Text) {
            if (old.attributes.text != n.attributes.text) {
                old.dom.nodeValue = n.attributes.text;
                old.attributes.text = n.attributes.text;
            }
            return old;
        }

        let diff: DiffRes = (new differ).map(old.attributes, n.attributes);

        for (let atr in diff) {
            let element: Element = old.dom

            if (diff[atr].type == DiffEnum.VALUE_CREATED || diff[atr].type == DiffEnum.VALUE_UPDATED) {
                old._setDomAttribute(element, atr, n.attributes[atr]);
            }

            if (diff[atr].type == DiffEnum.VALUE_DELETED) {
                element.removeAttribute(atr);
            }
        }
        NodeArraysDiffs(old.children, n.children)
        return old;
    }

    if (typeof (n.component) == "string") return;

    NodeDiffHandler(old.component.tree, n.component.render())

    return n;
}