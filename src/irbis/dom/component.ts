import { Irbis } from "../core";
import { HTMLIrbis, htmlUtils } from "./htmlUtils";
import { utils, objectDifferenceEnum } from "./utils"


export class IrbisBaseComponent {

   slot: IrbisBaseComponent[] = []
   props: any = {}
   irbis: Irbis = null

   dom: HTMLIrbis[] = null;
   _parent: IrbisBaseComponent;

   constructor() {
      this.created()
   }

   created() { }

   protected data() {
      return {};
   }
   setIrbis(irbis: Irbis) {
      this.irbis = irbis;
      this.slot.forEach((child) => { child.setIrbis(irbis) });
   }
   setSlot(slot: IrbisBaseComponent[]) {
      this.slot = slot
      slot.forEach(node => {
         node.setParent(this);
      });
   }

   setProps(props) {
      this.props = props;
   }

   renderSlot() {
      return this.slot
   }

   needUpdate() {
      this.irbis.needUpdate(this);
   }

   protected render(): IrbisBaseComponent {
      return null;
   }

   protected _render(): IrbisBaseComponent {
      let rendered = this.render();
      if (!rendered) return null;

      rendered.setIrbis(this.irbis);
      rendered.setParent(this);

      return rendered;
   }

   createDomElement(): HTMLIrbis[] {
      return []
   }

   setParent(parent: IrbisBaseComponent) {
      this._parent = parent;
   }

   name(): string {
      return this.constructor.name;
   }

   componentUpdate(updated: IrbisBaseComponent) {
      if (this.name() != updated.name()) {
         this._applyFullUpdate(updated);
         this.destroyProp()
         return;
      }
      this._applyPartUpdate(updated)
      this.destroyProp()
   }
   destroyProp() {
      this.props = {};
   }
   _applyPartUpdate(updated: IrbisBaseComponent) {
      throw new Error("Method not implemented.");
   }

   _applyFullUpdate(updated: IrbisBaseComponent) {
      let dom = this.dom[0];

      let rendered = updated.createDomElement();

      rendered.reverse().forEach(element => {
         htmlUtils.insertAfter(element, dom)
      });

      this.dom.forEach((node) => {
         node.remove();
      })
   }

   update() {
      console.warn("can only use  IrbisComponent");
   }

   destroy() {
      this.dom.forEach((el) => { el.remove() });
   }
}

export class IrbisComponent extends IrbisBaseComponent {
   rendered_root: IrbisBaseComponent;

   private _createProps(data): object {
      return new Proxy(data, {
         set: (target, prop, val) => {
            target[prop] = val;
            this.needUpdate();
            return true
         },
         get(target, prop) {
            return target[prop];
         }
      })
   }
   setProps(props) {
      this.props = this._createProps(
         Object.assign(props, this.data())
      );
   }

   createDomElement(): HTMLIrbis[] {
      this.rendered_root = this.render();
      this.rendered_root.setIrbis(this.irbis);
      this.rendered_root.setParent(this);

      this.dom = this.rendered_root.createDomElement();
      return this.dom;
   }

   _applyPartUpdate(updated: IrbisBaseComponent) {
      let updated_root = this._render();
      updated.dom = this.dom;

      this.rendered_root.componentUpdate(updated_root);
   }

   update() {
      let dom = this.rendered_root.dom

      let newRoot = this._render();
      this.rendered_root.componentUpdate(newRoot);
      this.rendered_root = newRoot;
   }
}

export class BasicComponent extends IrbisBaseComponent {
   private _tag: string = ""

   constructor(tag: string) {
      super();
      this._tag = tag;
   }

   createDomElement(): HTMLIrbis[] {
      this.dom = [document.createElement(this._tag)];

      this.renderSlot().forEach((child) => {
         let domChildren = child.createDomElement();
         domChildren.forEach(element => {
            this.dom[0].appendChild(element);
         });
      })

      for (let prop in this.props) {
         this._setDomAttribute(prop, this.props[prop], true);
      }

      return this.dom;
   }

   _setDomAttribute(name: string, value: any, add_events: boolean = false) {
      if (name.startsWith("on")) {
         if (add_events)
            this.dom[0].addEventListener(name.substr(2), value);
         return;
      }

      if (this._tag == 'input' && name == 'value') {
         this.dom[0].value = value;
         return
      }
      this.dom[0].setAttribute(name, value);
   }

   name(): string {
      return this.constructor.name + "|" + this._tag;
   }

   _applyPartUpdate(updated: IrbisBaseComponent) {
      updated.dom = this.dom;

      let diff = utils.objectDifference(this.props, updated.props);

      diff.forEach((d) => {
         if (d.type == objectDifferenceEnum.ADDED || d.type == objectDifferenceEnum.UPDATED) {
            this._setDomAttribute(d.key, updated.props[d.key]);
         }
         if (d.type == objectDifferenceEnum.DELETED) {
            this._deleteDomAttribute(d.key);
         }
      })

      _applyPartUpdateChildren(this, updated)
   }
   private _deleteDomAttribute(name: string) {
      this.dom[0].removeAttribute(name);
   }
}

export class TemplateComponent extends IrbisBaseComponent {
   constructor(children: IrbisBaseComponent[]) {
      super();
      this.setSlot(children);
   }

   createDomElement(): HTMLIrbis[] {
      this.dom = []

      this.renderSlot().forEach((child) => {
         let domChildren = child.createDomElement();
         domChildren.forEach(element => {
            this.dom.push(element);
         });
      })

      return this.dom;
   }

   _applyPartUpdate(updated: IrbisBaseComponent) {
      updated.dom = this.dom
      _applyPartUpdateChildren(this, updated);
   }
}


export class TextComponent extends IrbisBaseComponent {
   private text: string = ""

   constructor(text) {
      super();
      this.text = String(text);
   }

   createDomElement(): HTMLIrbis[] {
      this.dom = [document.createTextNode(this.text)];

      return this.dom;
   }

   _applyPartUpdate(updated: IrbisBaseComponent) {
      this.dom[0].textContent = updated.text;
      updated.dom = this.dom
   }
}

function _applyPartUpdateChildren(old: IrbisBaseComponent, updated: IrbisBaseComponent) {
   let oldChildren = old.slot;
   let updatedChildren = updated.slot;
   let min = Math.min(oldChildren.length, updatedChildren.length)
   let max = Math.max(oldChildren.length, updatedChildren.length)

   if (oldChildren.length == 0 && max != 0) {
      return old._parent._applyFullUpdate(updated._parent);
   }
   for (let i = 0; i < max; ++i) {
      if (i >= min) {
         if (oldChildren.length < updatedChildren.length) {
            // ADD

            // @todo This update may works incorrect
            console.warn("This update may works incorrect");
            
            console.log(old, updated);


            let children = updatedChildren[i].createDomElement();

            children.forEach((child) => {
               htmlUtils.insertAfter(child, updatedChildren[i - 1].dom.at(-1));
            })

         } else {
            // REMOVE
            oldChildren[i].destroy();
         }
         continue;
      }

      oldChildren[i].componentUpdate(updatedChildren[i]);
   }

}