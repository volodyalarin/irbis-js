export type HTMLIrbis = Element | Text;

let htmlUtils = {
    insertAfter(newNode: HTMLIrbis, referenceNode: HTMLIrbis) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    },
    insertBefore(newNode: HTMLIrbis, referenceNode: HTMLIrbis) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode);
    }
}

export { htmlUtils };