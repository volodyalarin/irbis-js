import { IrbisBaseComponent, IrbisComponent } from "../dom/component";

export class Irbis {
    dom_element: Element

    root: IrbisBaseComponent;
    TIMER_LOOP: number = 100;
    constructor(root: IrbisBaseComponent) {
        this.root = root;
        root.setIrbis(this)
    }

    needUpdate(component: IrbisBaseComponent) {
        console.log("+update")
        this.updates.add(component)
    }

    updateTimer: NodeJS.Timer

    updates: Set<IrbisBaseComponent> = new Set<IrbisBaseComponent>()

    update() {
        if (!this.updates.size) return;
        let err = false;
        console.log("updating " + this.updates.size);
        this.updates.forEach(element => {
            try {
                element.update();
            } catch (e) {
                console.error(e)
                err = true;
            }
        });
        
        this.updates.clear();
        if (err) {
            alert('Произошла ошибка обновления. Произойдет принудительная перерисовка.')
            this.forceUpdate();
        }
    }

    forceUpdate(): void {
        this.dom_element.innerHTML = '';
        let rendered = this.root.createDomElement();
        rendered.forEach((child) => {
            this.dom_element.appendChild(child);
        })
    }

    mount(selector: string): void {
        this.dom_element = document.querySelector(selector);

        this.forceUpdate();

        this.updateTimer = setInterval(() => { this.update() }, this.TIMER_LOOP);

    }
}
