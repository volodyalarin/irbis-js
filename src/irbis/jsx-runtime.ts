import { BasicComponent, IrbisBaseComponent, TextComponent, TemplateComponent } from "./dom/component";
import { IrbisVDOMNode } from "./dom/node"


type JSXchild = IrbisBaseComponent | string | number | null

export function jsxs(element: any, attributes: any = {}) {
    let children: JSXchild[] = attributes.children || [];

    delete attributes.children;

    return ComponentCreator(element, attributes, children);
}

export function jsx(element: any, attributes: any = {}) {
    let children: IrbisBaseComponent[] = attributes.children ? [attributes.children] : [];

    delete attributes.children;

    return ComponentCreator(element, attributes, children);
}


function ComponentCreator(element: any, attributes: any, childen: JSXchild[]): IrbisBaseComponent {
    let component: IrbisBaseComponent;
    if (typeof element == "string") {
        component = new BasicComponent(element)
    }
    else {
        component = new element();
    }

    let irbis_children: IrbisBaseComponent[] = childen.map((child) => {
        if (typeof child == "string" || typeof child == "number")
        {
            return new TextComponent(child);
        }
        if (typeof child == "undefined" || child === null)
        {
            return new TextComponent("");
        }
        if (Array.isArray(child)) {
            return new TemplateComponent(child)
        }

        return child

    })
    component.setProps(attributes);
    component.setSlot(irbis_children)

    return component;
}
